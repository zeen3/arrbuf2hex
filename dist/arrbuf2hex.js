(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const h = Uint8Array.of(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102);
    exports.h = h;
    const H = Uint8Array.of(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70);
    exports.H = H;
    function* arrbufchargen(arrbuf, octetOffset = 0, hLen = (arrbuf.byteLength - (octetOffset / 2)) * 2, upper = false) {
        const hex = upper ? H : h, bv = new Uint8Array(arrbuf, octetOffset >>> 1, hLen & 1 | octetOffset & 1
            ? hLen + 1 >>> 1
            : hLen >>> 1);
        for (let i = (octetOffset & 1), j = 0; j++ < hLen; i++)
            yield hex[i & 1
                ? bv[i >>> 1] & 15
                : bv[i >>> 1] >>> 4];
    }
    exports.arrbufchargen = arrbufchargen;
    exports.arrbufIter = arrbufchargen;
    const arrbuf2hex = (arr, octetOffset = ArrayBuffer.isView(arr) ? arr.byteOffset * 2 : 0, hLen = (arr.byteLength - (octetOffset / 2)) * 2, upper = false) => String.fromCharCode(...arrbufchargen(ArrayBuffer.isView(arr) ? arr.buffer : arr, ArrayBuffer.isView(arr)
        ? octetOffset + (arr.byteOffset * 2)
        : octetOffset, hLen, upper));
    exports.arrbuf2hex = arrbuf2hex;
    exports.arr2hex = arrbuf2hex;
    exports.buf2hex = arrbuf2hex;
    exports.default = arrbuf2hex;
});
//# sourceMappingURL=arrbuf2hex.js.map