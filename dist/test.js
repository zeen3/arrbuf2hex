(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./arrbuf2hex", "util"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const arrbuf2hex_1 = require("./arrbuf2hex");
    const util_1 = require("util");
    function test(pass, msg, abuf, octetOffset = void 0, hLen = void 0, upper = void 0) {
        const ts = arrbuf2hex_1.buf2hex(abuf, octetOffset, hLen, upper);
        console.count('Test');
        console.assert(pass(ts), msg);
        console.log(`  ${msg} ${pass(ts) ? '\u2705' : '\u274C'} \n${util_1.inspect({ abuf, octetOffset, hLen, upper, result: ts }, { colors: true })}`);
    }
    test(str => 'string' === typeof str, 'arrbuf2hex returns strings given a TypedArray (Uint8Array)', new Uint8Array(3));
    test(str => 'string' === typeof str, 'arrbuf2hex returns strings given a TypedArray (Int32Array)', new Int32Array(3));
    test(str => '10203' === str, 'arrbuf2hex concats the first octet when when octetOffset is 1', Uint8Array.of(1, 2, 3), 1);
    test(str => '01020' === str, 'arrbuf2hex concats the last octet when hLen is an odd number', Uint8Array.of(1, 2, 3), 0, 5);
    test(str => '10203' === str, 'arrbuf2hex concats to octets within range', Uint8Array.of(1, 2, 3, 4), 1, 5);
    test(str => '020' === str, 'arrbuf2hex concats exactly to within range', Uint8Array.of(1, 2, 3, 4), 2, 3);
    test(str => /^[0-9a-f]+$/.test(str), 'arrbuf2hex produces lowercase by default', Uint8Array.of(9, 10, 11, 12));
    test(str => /^[0-9A-F]+$/.test(str), 'arrbuf2hex produces uppercase when set specified', Uint8Array.of(9, 10, 11, 12, 13, 14, 15, 16, 240, 255), 0, 20, true);
    test(str => str.length === 6, 'arrbuf2hex works with shared array buffers', new SharedArrayBuffer(3));
});
//# sourceMappingURL=test.js.map