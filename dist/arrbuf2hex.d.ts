declare const h: Uint8Array;
declare const H: Uint8Array;
declare function arrbufchargen(arrbuf: ArrayBuffer | SharedArrayBuffer, octetOffset?: number, hLen?: number, upper?: boolean): IterableIterator<number>;
declare const arrbuf2hex: (arr: ArrayBuffer | SharedArrayBuffer | ArrayBufferView, octetOffset?: number | undefined, hLen?: number | undefined, upper?: boolean | undefined) => string;
export default arrbuf2hex;
export { H, h, arrbufchargen, arrbufchargen as arrbufIter, arrbuf2hex, arrbuf2hex as arr2hex, arrbuf2hex as buf2hex };
//# sourceMappingURL=arrbuf2hex.d.ts.map