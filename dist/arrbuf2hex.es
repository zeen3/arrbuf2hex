const h = Uint8Array.of(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102);
const H = Uint8Array.of(48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70);
function* arrbufchargen(arrbuf, octetOffset = 0, hLen = (arrbuf.byteLength - (octetOffset / 2)) * 2, upper = false) {
    const hex = upper ? H : h, bv = new Uint8Array(arrbuf, octetOffset >>> 1, hLen & 1 | octetOffset & 1
        ? hLen + 1 >>> 1
        : hLen >>> 1);
    for (let i = (octetOffset & 1), j = 0; j++ < hLen; i++)
        yield hex[i & 1
            ? bv[i >>> 1] & 15
            : bv[i >>> 1] >>> 4];
}
const arrbuf2hex = (arr, octetOffset = ArrayBuffer.isView(arr) ? arr.byteOffset * 2 : 0, hLen = (arr.byteLength - (octetOffset / 2)) * 2, upper = false) => String.fromCharCode(...arrbufchargen(ArrayBuffer.isView(arr) ? arr.buffer : arr, ArrayBuffer.isView(arr)
    ? octetOffset + (arr.byteOffset * 2)
    : octetOffset, hLen, upper));
export default arrbuf2hex;
export { H, h, arrbufchargen, arrbufchargen as arrbufIter, arrbuf2hex, arrbuf2hex as arr2hex, arrbuf2hex as buf2hex };
//# sourceMappingURL=arrbuf2hex.es.map