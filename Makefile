#!/bin/make
DIST_FOLDER=dist

Makefile: esnext umd .mkln

esnext:
	tsc -t ESNext -m ESNext
	for v in $(DIST_FOLDER)/*.js; do \
		sed \
			-e 's/.js.map/.es.map/g' \
			-e "s/from '\\(.\/[^']*\\)'/from '\\1.mjs'/g" \
			-e "s/from '\\(..\/[^']*\\)'/from '\\1.mjs'/g" \
			-e "s/import(\(['\"]\)\([^\1\]\)\1)/import(\\1\\2.es\\1)/g" \
			-i "$$v" ; \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js).es" ; done
	for v in $(DIST_FOLDER)/*.js.map; do \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js.map).es.map" ; done

umd:
	tsc -m umd -t ES2017

amd:
	tsc -m amd -t ES2017

cjs:
	tsc -m commonjs -t ES2017

none:
	tsc -m none 

.mkln:
	for v in $(DIST_FOLDER)/*.js $(DIST_FOLDER)/*.es $(DIST_FOLDER)/*.js.map $(DIST_FOLDER)/*.es.map; do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v")"; fi; done;
	for v in $(DIST_FOLDER)/*.es; do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v" .es).mjs"; fi; done;

clrsm:
	symlinks -rd .

include clrsm
