# arrbuf2hex

fast custom array buffer to hexadecimal string function for when you need
hexadecimal numbers out of a buffer and they don't always align.

also practiacally all the functions in this are bitwise so uh it's nigh instant.

use:

```js
const { buf2hex } = require('arrbuf2hex')
buf2hex(new ArrayBuffer(8), 0, 3, true)
// 000
buf2hex(Uint8Array.of(0xff, 0xf0, 0x0f), 1, 3)
// ff0
buf2hex(Uint8Array.of(0xff, 0xf0, 0x0f), 0, 5, true)
// fff00
```

or

```es
import { buf2hex } from 'arrbuf2hex'
buf2hex(new ArrayBuffer(8), 0, 3, true)
// 000
```

in the browser:

```es
import { buf2hex } from '/node_modules/arrbuf2hex/arrbuf2hex.es'
crypto.subtle.digest('SHA-1', arrbuf).then(buf2hex).then(hash => {
  // print hash
})
(async () => {
  const hash = buf2hex(await crypto.subtle.digest('SHA-1', new ArrayBuffer(0)))
  console.assert("da39a3ee5e6b4b0d3255bfef95601890afd80709" === hash, `SHA-1 invalid, zero returns ${hash} instead of da39a3ee5e6b4b0d3255bfef95601890afd80709`)
})
```

